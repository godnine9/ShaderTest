// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/PBRShader_v3"
{
	Properties
	{
		_Albedo1("Albedo", 2D) = "white" {}
		[Normal]_Normal1("Normal", 2D) = "bump" {}
		_PBR_Mix("PBR_Mix", 2D) = "white" {}
		_NormalScale1("NormalScale", Range( 1 , 5)) = 0
		_AOScale1("AOScale", Range( 0.1 , 2)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _NormalScale1;
		uniform sampler2D _Normal1;
		uniform float4 _Normal1_ST;
		uniform sampler2D _Albedo1;
		uniform float4 _Albedo1_ST;
		uniform sampler2D _PBR_Mix;
		uniform float4 _PBR_Mix_ST;
		uniform float _AOScale1;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal1 = i.uv_texcoord * _Normal1_ST.xy + _Normal1_ST.zw;
			float3 Normal21 = UnpackScaleNormal( tex2D( _Normal1, uv_Normal1 ), _NormalScale1 );
			o.Normal = Normal21;
			float2 uv_Albedo1 = i.uv_texcoord * _Albedo1_ST.xy + _Albedo1_ST.zw;
			float4 tex2DNode9 = tex2D( _Albedo1, uv_Albedo1 );
			float3 Albedo20 = (tex2DNode9).rgb;
			o.Albedo = Albedo20;
			float2 uv_PBR_Mix = i.uv_texcoord * _PBR_Mix_ST.xy + _PBR_Mix_ST.zw;
			float4 tex2DNode31 = tex2D( _PBR_Mix, uv_PBR_Mix );
			float Metallic17 = tex2DNode31.b;
			o.Metallic = Metallic17;
			float Smoothness18 = tex2DNode31.g;
			o.Smoothness = Smoothness18;
			float AO19 = (( _AOScale1 * -1.0 ) + (tex2DNode31.r - 0.0) * (_AOScale1 - ( _AOScale1 * -1.0 )) / (1.0 - 0.0));
			o.Occlusion = AO19;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
2074;153;1692;817;1362.306;80.69036;1.394278;True;True
Node;AmplifyShaderEditor.CommentaryNode;11;-903.2813,355.6814;Inherit;False;849.316;353.4089;Comment;5;29;28;24;19;31;AO;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;7;-912.1381,-374.8243;Inherit;False;860.1548;280;Comment;4;27;20;16;9;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;8;-921.5643,-45.44394;Inherit;False;894.5071;363.9227;Comment;3;21;14;10;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-881.5239,614.3;Inherit;False;Property;_AOScale1;AOScale;4;0;Create;True;0;0;True;0;1;1.022;0.1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;9;-862.1381,-324.8243;Inherit;True;Property;_Albedo1;Albedo;0;0;Create;True;0;0;False;0;-1;None;efd0eee9e4394a94db1426ce0bb7a902;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;10;-894.9755,61.47885;Inherit;False;Property;_NormalScale1;NormalScale;3;0;Create;True;0;0;False;0;0;1;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;31;-871.8425,409.3466;Inherit;True;Property;_PBR_Mix;PBR_Mix;2;0;Create;True;0;0;False;0;-1;None;9fb865a65b1c9ff4dacff98c0f3b2bc4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-592.7314,609.6016;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;16;-548.4362,-323.8087;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;14;-594.5643,20.55606;Inherit;True;Property;_Normal1;Normal;1;1;[Normal];Create;True;0;0;False;0;-1;None;00e0021f9f2d77a4fb3d5957267488e7;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;28;-504.3908,411.8187;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-3;False;4;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-535.9667,781.0621;Inherit;False;Metallic;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21;-270.0573,47.02997;Inherit;False;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;18;-543.2919,994.9554;Inherit;False;Smoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;20;-294.9836,-324.525;Inherit;False;Albedo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19;-239.856,410.8139;Inherit;False;AO;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;30;231.7056,485.1274;Inherit;False;18;Smoothness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;22;230.2404,384.0409;Inherit;False;17;Metallic;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;244.8905,126.1969;Inherit;False;20;Albedo;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;234.6355,294.6745;Inherit;False;19;AO;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;241.9606,219.9584;Inherit;False;21;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-296.2472,-229.3154;Inherit;False;Opacity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;476.8872,144.3469;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/PBRShader_v3;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;29;0;24;0
WireConnection;16;0;9;0
WireConnection;14;5;10;0
WireConnection;28;0;31;1
WireConnection;28;3;29;0
WireConnection;28;4;24;0
WireConnection;17;0;31;3
WireConnection;21;0;14;0
WireConnection;18;0;31;2
WireConnection;20;0;16;0
WireConnection;19;0;28;0
WireConnection;27;0;9;4
WireConnection;0;0;23;0
WireConnection;0;1;26;0
WireConnection;0;3;22;0
WireConnection;0;4;30;0
WireConnection;0;5;25;0
ASEEND*/
//CHKSM=00F7FA0C62043D36311C59F9A7B9900211FD7FC3
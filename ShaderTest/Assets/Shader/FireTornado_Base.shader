// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/FireTornado_Base"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0
		_FireIntensity("FireIntensity", Range( 1 , 3)) = 1.498667
		_FireColor("FireColor", Color) = (1,0.5928419,0,0)
		_FireColor2("FireColor2", Color) = (1,0.5928419,0,0)
		_FireTexture("FireTexture", 2D) = "white" {}
		_DissolveAmount("Dissolve Amount", Range( 0 , 1)) = 0
		_Waves("Waves", Float) = 1
		_TwistSpeedX("TwistSpeedX", Float) = 3
		_TwistPowerX("TwistPowerX", Float) = 1
		_TwistPowerZ("TwistPowerZ", Float) = 2
		_TwisSpeedZ("TwisSpeedZ", Float) = 2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Overlay+0" "IsEmissive" = "true"  }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float _Waves;
		uniform float _TwistSpeedX;
		uniform float _TwisSpeedZ;
		uniform float _TwistPowerZ;
		uniform float _TwistPowerX;
		uniform sampler2D _FireTexture;
		uniform float4 _FireTexture_ST;
		uniform float _FireIntensity;
		uniform float4 _FireColor2;
		uniform float4 _FireColor;
		uniform float _DissolveAmount;
		uniform float _Cutoff = 0;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertexNormal = v.normal.xyz;
			float V151 = v.texcoord.xyz.y;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float TwistSpeedX113 = ( _Time.y * _TwistSpeedX );
			float TempOut111 = ( V151 * 1.0 );
			float TwistPowerZ112 = ( _Time.y * _TwistPowerX );
			float3 appendResult110 = (float3(( TempOut111 * sin( ( TwistPowerZ112 + ase_vertex3Pos.y ) ) ) , 0.0 , ( TempOut111 * sin( ( TwistPowerZ112 + ase_vertex3Pos.y + ( 0.5 * UNITY_PI ) ) ) )));
			v.vertex.xyz += ( ( ase_vertexNormal * ( V151 * sin( ( _Waves * ( ase_vertex3Pos.y + TwistSpeedX113 ) ) ) * UNITY_PI ) * _TwisSpeedZ ) + ( _TwistPowerZ * appendResult110 ) );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv0_FireTexture = i.uv_texcoord * _FireTexture_ST.xy + _FireTexture_ST.zw;
			float2 panner13 = ( ( _Time.x * 5.0 ) * float2( -1,-3 ) + uv0_FireTexture);
			float4 tex2DNode3 = tex2D( _FireTexture, panner13 );
			float temp_output_9_0 = ( (0.1 + (tex2DNode3.r - 0.0) * (1.0 - 0.1) / (1.0 - 0.0)) * _FireIntensity );
			float4 lerpResult152 = lerp( ( ( 1.0 - temp_output_9_0 ) * _FireColor2 ) , ( temp_output_9_0 * _FireColor ) , temp_output_9_0);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV5 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode5 = ( 0.0 + 1.6 * pow( 1.0 - fresnelNdotV5, 2.45 ) );
			o.Emission = ( lerpResult152 + fresnelNode5 ).rgb;
			o.Alpha = 1;
			clip( ( tex2DNode3.r + (-0.6 + (( 1.0 - _DissolveAmount ) - 0.0) * (0.6 - -0.6) / (1.0 - 0.0)) ) - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
2070;91;1334;774;2091.841;-714.313;1.74564;True;False
Node;AmplifyShaderEditor.CommentaryNode;123;-1730.477,1649.213;Inherit;False;729.5251;437.0408;Comment;7;93;112;91;113;90;88;92;Twist;1,1,1,1;0;0
Node;AmplifyShaderEditor.TimeNode;90;-1680.477,1699.213;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;88;-1663.03,1871.442;Inherit;False;Property;_TwistSpeedX;TwistSpeedX;7;0;Create;True;0;0;False;0;3;-1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;14;-2162.855,410.8238;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;92;-1658.55,1963.763;Inherit;False;Property;_TwistPowerX;TwistPowerX;8;0;Create;True;0;0;False;0;1;6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-1894.883,445.3885;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-2213.928,206.9048;Inherit;False;0;3;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;-1393.799,1703.429;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;-1388.075,1902.771;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;139;-1659.028,1155.594;Inherit;False;0;3;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;13;-1859.273,220.6584;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-3;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;113;-1243.952,1703.974;Inherit;False;TwistSpeedX;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;151;-1412.75,1218.299;Inherit;False;V;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;122;-408.0697,1573.436;Inherit;False;1185.783;591.644;Comment;11;95;110;101;103;116;98;96;102;117;115;118;AppendResult;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;112;-1235.822,1918.049;Inherit;False;TwistPowerZ;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-1607.729,194.4835;Inherit;True;Property;_FireTexture;FireTexture;4;0;Create;True;0;0;False;0;-1;1f256e8a66ea42c4ea6aa1c16a11c9e7;7aad8c583ef292e48b06af0d1f2fab97;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;144;-650.2657,1654.224;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;149;-1642.266,1457.917;Inherit;False;113;TwistSpeedX;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;-1283.49,1099.247;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;115;-398.1003,1626.051;Inherit;False;112;TwistPowerZ;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.PiNode;102;-289.4467,2055.08;Inherit;False;1;0;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;118;-304.2894,1964.77;Inherit;False;112;TwistPowerZ;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;101;-30.0666,1909.885;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;96;-151.7426,1626.779;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;111;-1102.767,1134.936;Inherit;False;TempOut;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-1277.646,442.9422;Inherit;False;Property;_FireIntensity;FireIntensity;1;0;Create;True;0;0;False;0;1.498667;2.55;1;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;125;-1632.002,1363.965;Inherit;False;Property;_Waves;Waves;6;0;Create;True;0;0;False;0;1;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;127;-1364.435,1444.074;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;12;-1196.724,195.6006;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;126;-1195.019,1378.584;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-921.4722,162.9476;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;95;-18.61748,1670.651;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;103;112.1529,1940.594;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;116;-383.1776,1749.882;Inherit;False;111;TempOut;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;8;-958.3729,413.3343;Inherit;False;Property;_FireColor;FireColor;2;0;Create;True;0;0;False;0;1,0.5928419,0,0;0.8679245,0.3551753,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PiNode;130;-1376.542,1320.513;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-1962.527,717.8918;Float;False;Property;_DissolveAmount;Dissolve Amount;5;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;98;107.285,1701.357;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;146.2827,1810.945;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;154;-815.4322,803.6015;Inherit;False;Property;_FireColor2;FireColor2;3;0;Create;True;0;0;False;0;1,0.5928419,0,0;1,0.6084825,0.5424528,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;153;-768.5173,560.7483;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;129;-1049.587,1388.315;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-599.1599,181.6377;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;29;-1638.376,927.5784;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;155;-544.982,657.3376;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;145;-948.0978,1249.254;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;17;-1569.166,711.5927;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;156;-1059.957,1479.86;Inherit;False;Property;_TwisSpeedZ;TwisSpeedZ;10;0;Create;True;0;0;False;0;2;0.02;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;133;-785.0239,1454.346;Inherit;False;Property;_TwistPowerZ;TwistPowerZ;9;0;Create;True;0;0;False;0;2;0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;110;317.5782,1791.307;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FresnelNode;5;-958.4098,-107.0304;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1.6;False;3;FLOAT;2.45;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;152;-485.6484,410.3449;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;148;-719.186,1166.688;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;137;-486.0406,1304.135;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;18;-1355.283,682.7028;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.6;False;4;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;135;-428.0385,1019.142;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;19;-1024.639,610.4528;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;11;-297.7635,182.85;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;120;-1152.561,933.4849;Inherit;False;NormalY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/FireTornado_Base;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0;True;False;0;True;Transparent;;Overlay;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;14;1
WireConnection;91;0;90;2
WireConnection;91;1;88;0
WireConnection;93;0;90;2
WireConnection;93;1;92;0
WireConnection;13;0;15;0
WireConnection;13;1;20;0
WireConnection;113;0;91;0
WireConnection;151;0;139;2
WireConnection;112;0;93;0
WireConnection;3;1;13;0
WireConnection;100;0;151;0
WireConnection;101;0;118;0
WireConnection;101;1;144;2
WireConnection;101;2;102;0
WireConnection;96;0;115;0
WireConnection;96;1;144;2
WireConnection;111;0;100;0
WireConnection;127;0;144;2
WireConnection;127;1;149;0
WireConnection;12;0;3;1
WireConnection;126;0;125;0
WireConnection;126;1;127;0
WireConnection;9;0;12;0
WireConnection;9;1;10;0
WireConnection;95;0;96;0
WireConnection;103;0;101;0
WireConnection;98;0;116;0
WireConnection;98;1;95;0
WireConnection;117;0;116;0
WireConnection;117;1;103;0
WireConnection;153;0;9;0
WireConnection;129;0;126;0
WireConnection;6;0;9;0
WireConnection;6;1;8;0
WireConnection;155;0;153;0
WireConnection;155;1;154;0
WireConnection;145;0;151;0
WireConnection;145;1;129;0
WireConnection;145;2;130;0
WireConnection;17;0;16;0
WireConnection;110;0;98;0
WireConnection;110;2;117;0
WireConnection;152;0;155;0
WireConnection;152;1;6;0
WireConnection;152;2;9;0
WireConnection;148;0;29;0
WireConnection;148;1;145;0
WireConnection;148;2;156;0
WireConnection;137;0;133;0
WireConnection;137;1;110;0
WireConnection;18;0;17;0
WireConnection;135;0;148;0
WireConnection;135;1;137;0
WireConnection;19;0;3;1
WireConnection;19;1;18;0
WireConnection;11;0;152;0
WireConnection;11;1;5;0
WireConnection;2;2;11;0
WireConnection;2;10;19;0
WireConnection;2;11;135;0
ASEEND*/
//CHKSM=D385C648D96E1B39979069156BB460379F721B76
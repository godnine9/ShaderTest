// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UIShiny"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_MaskTexture1("MaskTexture", 2D) = "white" {}
		_ShinyTexture("ShinyTexture", 2D) = "white" {}
		_TimeScale("TimeScale", Range( 1 , 3)) = 1
		_ShinyPower("ShinyPower", Range( 0 , 5)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float _TimeScale;
			uniform sampler2D _ShinyTexture;
			uniform float4 _ShinyTexture_ST;
			uniform float _ShinyPower;
			uniform sampler2D _MaskTexture1;
			uniform float4 _MaskTexture1_ST;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				float2 uv_MainTex = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode5 = tex2D( _MainTex, uv_MainTex );
				float mulTime28 = _Time.y * _TimeScale;
				float clampResult30 = clamp( sin( mulTime28 ) , 0.0 , 1.0 );
				float2 uv_ShinyTexture = IN.texcoord.xy * _ShinyTexture_ST.xy + _ShinyTexture_ST.zw;
				float4 BaseTexture10 = ( ( tex2DNode5 * _Color ) + ( clampResult30 * tex2D( _ShinyTexture, uv_ShinyTexture ) * _ShinyPower ) );
				float2 uv_MaskTexture1 = IN.texcoord.xy * _MaskTexture1_ST.xy + _MaskTexture1_ST.zw;
				float Alpha11 = ( tex2D( _MaskTexture1, uv_MaskTexture1 ).r * tex2DNode5.a );
				float4 appendResult15 = (float4(BaseTexture10.rgb , Alpha11));
				
				half4 color = appendResult15;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17800
2261;149;1334;774;3333.524;798.7034;3.18316;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-2398.886,455.4776;Inherit;False;1082.55;597.3046;Shiny;7;17;19;21;23;26;28;30;Shiny;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2375.567,553.6937;Inherit;False;Property;_TimeScale;TimeScale;2;0;Create;True;0;0;False;0;1;2.12;1;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;28;-2206.472,650.7751;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;2;-2406.669,-163.888;Inherit;False;1437.033;568.0354;Comment;9;11;10;8;7;6;5;4;3;27;Base Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.SinOpNode;19;-2051.238,606.2428;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;3;-2388.58,-97.00732;Inherit;False;0;0;_MainTex;Shader;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;17;-1934.428,743.2509;Inherit;True;Property;_ShinyTexture;ShinyTexture;1;0;Create;True;0;0;False;0;-1;None;4b826b3ff2794594ea8eefd5d4c21ad7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;23;-1853.6,957.3591;Inherit;False;Property;_ShinyPower;ShinyPower;3;0;Create;True;0;0;False;0;1;0.38;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;30;-1905.826,584.8795;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;4;-1745.983,-33.20862;Inherit;False;0;0;_Color;Shader;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5;-2211.003,-101.9646;Inherit;True;Property;_TextureSample5;Texture Sample 4;2;0;Create;True;0;0;False;0;-1;None;de92c96f8570409488905bdbb9eedaaf;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-1537.272,-89.68352;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1604.404,561.5513;Inherit;False;3;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;7;-2208.889,158.6366;Inherit;True;Property;_MaskTexture1;MaskTexture;0;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-1357.061,51.66925;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-1923.686,153.3185;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-1218.707,-106.4674;Inherit;False;BaseTexture;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-1762.678,144.103;Inherit;False;Alpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;12;-933.1212,-166.0276;Inherit;False;869.2437;372.2335;Comment;4;15;14;13;0;FinalOutput;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;13;-883.1214,-116.0275;Inherit;False;10;BaseTexture;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;14;-873.3219,118.206;Inherit;False;11;Alpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;15;-544.3641,-85.82648;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-376.6347,-97.64603;Float;False;True;-1;2;ASEMaterialInspector;0;4;UIShiny;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;True;2;False;-1;True;True;True;True;True;0;True;-9;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;0
WireConnection;28;0;21;0
WireConnection;19;0;28;0
WireConnection;30;0;19;0
WireConnection;5;0;3;0
WireConnection;6;0;5;0
WireConnection;6;1;4;0
WireConnection;26;0;30;0
WireConnection;26;1;17;0
WireConnection;26;2;23;0
WireConnection;27;0;6;0
WireConnection;27;1;26;0
WireConnection;8;0;7;1
WireConnection;8;1;5;4
WireConnection;10;0;27;0
WireConnection;11;0;8;0
WireConnection;15;0;13;0
WireConnection;15;3;14;0
WireConnection;0;0;15;0
ASEEND*/
//CHKSM=605D45126B22F92CF5E343603378772AF6C9ACC6
// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/PBRShader_v2"
{
	Properties
	{
		_Albedo2("Albedo", 2D) = "white" {}
		[Normal]_Normal2("Normal", 2D) = "bump" {}
		_Metallic2("Metallic", 2D) = "white" {}
		_Roughness2("Roughness", 2D) = "white" {}
		_Occlusion2("Occlusion", 2D) = "white" {}
		_NormalScale2("NormalScale", Range( 1 , 5)) = 0
		_AOScale2("AOScale", Range( 0.1 , 2)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _AOScale2;
		uniform float _NormalScale2;
		uniform sampler2D _Normal2;
		uniform float4 _Normal2_ST;
		uniform sampler2D _Albedo2;
		uniform float4 _Albedo2_ST;
		uniform sampler2D _Metallic2;
		uniform float4 _Metallic2_ST;
		uniform sampler2D _Roughness2;
		uniform float4 _Roughness2_ST;
		uniform sampler2D _Occlusion2;
		uniform float4 _Occlusion2_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal2 = i.uv_texcoord * _Normal2_ST.xy + _Normal2_ST.zw;
			float3 Normal21 = UnpackScaleNormal( tex2D( _Normal2, uv_Normal2 ), _NormalScale2 );
			o.Normal = Normal21;
			float2 uv_Albedo2 = i.uv_texcoord * _Albedo2_ST.xy + _Albedo2_ST.zw;
			float4 tex2DNode9 = tex2D( _Albedo2, uv_Albedo2 );
			float3 Albedo20 = (tex2DNode9).rgb;
			o.Albedo = Albedo20;
			float2 uv_Metallic2 = i.uv_texcoord * _Metallic2_ST.xy + _Metallic2_ST.zw;
			float4 Metallic17 = tex2D( _Metallic2, uv_Metallic2 );
			o.Metallic = Metallic17.r;
			float2 uv_Roughness2 = i.uv_texcoord * _Roughness2_ST.xy + _Roughness2_ST.zw;
			float4 Smoothness18 = tex2D( _Roughness2, uv_Roughness2 );
			o.Smoothness = Smoothness18.r;
			float2 uv_Occlusion2 = i.uv_texcoord * _Occlusion2_ST.xy + _Occlusion2_ST.zw;
			float4 tex2DNode13 = tex2D( _Occlusion2, uv_Occlusion2 );
			float AO19 = tex2DNode13.r;
			o.Occlusion = AO19;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
2074;153;1692;817;772.3491;507.698;1.388126;True;True
Node;AmplifyShaderEditor.CommentaryNode;7;-922.4988,-395.0438;Inherit;False;860.1548;280;Comment;4;27;20;16;9;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;8;-931.9249,-65.66342;Inherit;False;894.5071;363.9227;Comment;3;21;14;10;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;9;-872.4988,-345.0439;Inherit;True;Property;_Albedo2;Albedo;0;0;Create;True;0;0;False;0;-1;None;efd0eee9e4394a94db1426ce0bb7a902;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;10;-905.3362,41.25935;Inherit;False;Property;_NormalScale2;NormalScale;5;0;Create;True;0;0;False;0;0;1;1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;11;-913.642,335.4619;Inherit;False;849.316;353.4089;Comment;5;29;28;24;19;13;AO;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;15;-868.5309,742.5029;Inherit;True;Property;_Metallic2;Metallic;2;0;Create;True;0;0;False;0;-1;None;d4940c862b9d0f349b3eec6e0da05578;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;12;-881.4667,974.663;Inherit;True;Property;_Roughness2;Roughness;3;0;Create;True;0;0;False;0;-1;None;b97db8acddac10d4c867939fcd38e487;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;13;-863.642,385.4619;Inherit;True;Property;_Occlusion2;Occlusion;4;0;Create;True;0;0;False;0;-1;None;120ede8c4897abf488ebbcd0d0b46ede;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;14;-604.925,0.3365791;Inherit;True;Property;_Normal2;Normal;1;1;[Normal];Create;True;0;0;False;0;-1;None;00e0021f9f2d77a4fb3d5957267488e7;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;16;-558.7969,-344.0282;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;21;-280.4179,26.81048;Inherit;False;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;20;-305.3443,-344.7445;Inherit;False;Albedo;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19;-257.8766,401.5371;Inherit;False;AO;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;18;-553.6525,974.736;Inherit;False;Smoothness;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-546.3273,760.8428;Inherit;False;Metallic;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;231.6001,199.7388;Inherit;False;21;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-306.6078,-249.5349;Inherit;False;Opacity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;28;-514.7513,391.5992;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-3;False;4;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;224.275,274.455;Inherit;False;19;AO;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;30;221.3451,464.9079;Inherit;False;18;Smoothness;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;234.53,105.9774;Inherit;False;20;Albedo;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;22;219.8799,363.8213;Inherit;False;17;Metallic;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-603.092,589.3821;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-891.8846,594.0806;Inherit;False;Property;_AOScale2;AOScale;6;0;Create;True;0;0;True;0;1;1;0.1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;464.8872,74.34686;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Custom/PBRShader_v2;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;14;5;10;0
WireConnection;16;0;9;0
WireConnection;21;0;14;0
WireConnection;20;0;16;0
WireConnection;19;0;13;1
WireConnection;18;0;12;0
WireConnection;17;0;15;0
WireConnection;27;0;9;4
WireConnection;28;0;13;1
WireConnection;28;3;29;0
WireConnection;28;4;24;0
WireConnection;29;0;24;0
WireConnection;0;0;23;0
WireConnection;0;1;26;0
WireConnection;0;3;22;0
WireConnection;0;4;30;0
WireConnection;0;5;25;0
ASEEND*/
//CHKSM=AE827F07C79733DC456662852AFAEF12807B0D80
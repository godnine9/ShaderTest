// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/GhostShader"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_AbedoTint("AbedoTint", Color) = (1,0.9325914,0.3632075,0)
		_RimMask("RimMask", 2D) = "white" {}
		_DisolveGuide("Disolve Guide", 2D) = "white" {}
		_RimMaskSpeed("RimMaskSpeed", Range( -5 , 5)) = 1
		_IceValue("IceValue", Range( 0 , 1)) = 0
		_FresnelScale("Fresnel Scale", Float) = 0
		_BurnRamp("Burn Ramp", 2D) = "white" {}
		_FresnelPower("Fresnel Power", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 4.6
		#pragma surface surf StandardSpecular keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _AbedoTint;
		uniform float _FresnelScale;
		uniform float _FresnelPower;
		uniform sampler2D _RimMask;
		uniform float _RimMaskSpeed;
		uniform float4 _RimMask_ST;
		uniform float _IceValue;
		uniform sampler2D _DisolveGuide;
		uniform float4 _DisolveGuide_ST;
		uniform sampler2D _BurnRamp;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float3 BaseColorTint419 = (_AbedoTint).rgb;
			o.Albedo = ( (tex2D( _Albedo, uv_Albedo )).rgb * BaseColorTint419 );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV344 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode344 = ( 0.0 + _FresnelScale * pow( 1.0 - fresnelNdotV344, _FresnelPower ) );
			float Time300 = _Time.y;
			float2 uv0_RimMask = i.uv_texcoord * _RimMask_ST.xy + _RimMask_ST.zw;
			float3 RimMask312 = (tex2D( _RimMask, ( ( _RimMaskSpeed * Time300 ) + uv0_RimMask ) )).rgb;
			float3 FresnelColor365 = ( (float3( 0,0,0 ) + (BaseColorTint419 - float3( 0,0,0 )) * (float3( 0.8,0.8,0.8 ) - float3( 0,0,0 )) / (float3( 1,1,1 ) - float3( 0,0,0 ))) * fresnelNode344 * RimMask312 );
			float3 Opacity352 = ( fresnelNode344 * RimMask312 );
			float3 lerpResult367 = lerp( BaseColorTint419 , FresnelColor365 , Opacity352);
			float3 clampResult415 = clamp( lerpResult367 , float3( 0,0,0 ) , float3( 1,1,1 ) );
			float3 Emission360 = clampResult415;
			float2 uv_DisolveGuide = i.uv_texcoord * _DisolveGuide_ST.xy + _DisolveGuide_ST.zw;
			float Mask441 = ( (-0.6 + (( 1.0 - _IceValue ) - 0.0) * (0.6 - -0.6) / (1.0 - 0.0)) + tex2D( _DisolveGuide, uv_DisolveGuide ).r );
			float clampResult434 = clamp( (-4.0 + (Mask441 - 0.0) * (4.0 - -4.0) / (1.0 - 0.0)) , 0.0 , 1.0 );
			float temp_output_433_0 = ( 1.0 - clampResult434 );
			float2 appendResult432 = (float2(temp_output_433_0 , 0.0));
			float4 lerpResult425 = lerp( float4( Emission360 , 0.0 ) , ( temp_output_433_0 * tex2D( _BurnRamp, appendResult432 ) ) , temp_output_433_0);
			o.Emission = lerpResult425.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
1943;114;1293;839;2068.518;1651.539;2.385916;True;False
Node;AmplifyShaderEditor.CommentaryNode;297;-2549.785,-1919.807;Inherit;False;513.7285;309.5243;Time;2;300;299;Time;1,1,1,1;0;0
Node;AmplifyShaderEditor.TimeNode;299;-2497.52,-1798.481;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;302;-2563.594,20.11819;Inherit;False;1714.812;440.3904;RimMask;8;312;311;310;309;308;307;306;303;RimMask;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;300;-2275.814,-1744.085;Inherit;False;Time;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;306;-2207.58,200.3862;Inherit;False;300;Time;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;303;-2493.477,68.93487;Float;False;Property;_RimMaskSpeed;RimMaskSpeed;4;0;Create;True;0;0;False;0;1;-0.5;-5;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;308;-1985.756,148.1096;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;307;-2053.319,299.9014;Inherit;False;0;310;2;3;2;SAMPLER2D;;False;0;FLOAT2;5,5;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;429;-1017.915,-600.4464;Inherit;False;1298.231;484.0652;Dissolve - Opacity Mask;7;443;442;441;440;439;438;437;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;442;-995.1941,-541.8904;Float;False;Property;_IceValue;IceValue;5;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;309;-1807.214,163.5372;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;412;-898.9153,-1835.655;Inherit;False;Property;_AbedoTint;AbedoTint;1;0;Create;True;0;0;False;0;1,0.9325914,0.3632075,0;1,0.6206582,0.0235849,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;413;-554.7134,-1839.655;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;310;-1634.282,210.173;Inherit;True;Property;_RimMask;RimMask;2;0;Create;True;0;0;False;0;-1;None;2ae47013e33192942bf42d1f93135e91;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;443;-929.2012,-415.58;Inherit;True;Property;_DisolveGuide;Disolve Guide;3;0;Create;True;0;0;False;0;-1;999fa7824910a6a4f8d33c1cb629a1d2;999fa7824910a6a4f8d33c1cb629a1d2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;440;-705.7892,-527.3859;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;339;-2536.897,-798.1902;Inherit;False;1372.065;722.4354;;10;342;341;340;345;346;352;365;343;344;423;Adding Rim effect;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;419;-349.4438,-1710.369;Inherit;False;BaseColorTint;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;311;-1342.269,192.7639;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;438;-279.9954,-264.1581;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;439;-524.6381,-530.0903;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.6;False;4;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;342;-2504.249,-734.1536;Inherit;False;419;BaseColorTint;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;341;-2516.385,-295.4472;Float;False;Property;_FresnelPower;Fresnel Power;8;0;Create;True;0;0;False;0;0;1.96;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;340;-2499.428,-423.2101;Float;False;Property;_FresnelScale;Fresnel Scale;6;0;Create;True;0;0;False;0;0;6.79;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;312;-1161.813,194.418;Inherit;False;RimMask;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;437;-218.3183,-532.7955;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;430;-1033.404,-1097.722;Inherit;False;1304.535;470.0397;Burn Effect - Emission;7;431;436;432;433;434;435;444;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;441;-6.495841,-533.158;Inherit;True;Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;344;-2267.972,-453.025;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;343;-2157.655,-199.0105;Inherit;False;312;RimMask;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;423;-2212.581,-688.7442;Inherit;False;5;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0.8,0.8,0.8;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;345;-1825.425,-560.3679;Inherit;True;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;-1841.769,-303.3553;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;1,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;444;-993.1766,-983.9078;Inherit;False;441;Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;435;-862.1484,-890.6238;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-4;False;4;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;361;-2610.512,-1517.57;Inherit;False;1383.229;666.2169;Comment;6;360;366;369;367;398;415;Emission Mix;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;365;-1485.019,-548.293;Inherit;False;FresnelColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;352;-1490.819,-399.7487;Inherit;False;Opacity;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;369;-2560.834,-1398.733;Inherit;True;419;BaseColorTint;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;398;-2574.456,-1179.96;Inherit;False;365;FresnelColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;366;-2561.86,-1025.523;Inherit;False;352;Opacity;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;434;-778.2064,-1051.305;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;367;-2193.007,-1245.245;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;433;-549.774,-1057.392;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;432;-573.2668,-848.412;Inherit;True;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ClampOpNode;415;-1924.364,-1247.447;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;406;-881.2375,-2084.162;Inherit;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;-1;None;4a9be1a884a2e3b449b52a5626a08d4f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;360;-1763.189,-1239.254;Inherit;False;Emission;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;436;-331.7341,-828.8541;Inherit;True;Property;_BurnRamp;Burn Ramp;7;0;Create;True;0;0;False;0;-1;37980e2eac874ba46937ed6622ca6567;37980e2eac874ba46937ed6622ca6567;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;1,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;121;-646.1723,-1577.241;Inherit;True;360;Emission;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;408;-556.2375,-2012.162;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;431;-190.2812,-1024.119;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;411;-283.7134,-1893.655;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;425;-334.8836,-1547.569;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;404;-29.6559,-1725.288;Float;False;True;-1;6;ASEMaterialInspector;0;0;StandardSpecular;Custom/GhostShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Translucent;0.1;True;False;0;False;Opaque;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;1;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;300;0;299;2
WireConnection;308;0;303;0
WireConnection;308;1;306;0
WireConnection;309;0;308;0
WireConnection;309;1;307;0
WireConnection;413;0;412;0
WireConnection;310;1;309;0
WireConnection;440;0;442;0
WireConnection;419;0;413;0
WireConnection;311;0;310;0
WireConnection;438;0;443;1
WireConnection;439;0;440;0
WireConnection;312;0;311;0
WireConnection;437;0;439;0
WireConnection;437;1;438;0
WireConnection;441;0;437;0
WireConnection;344;2;340;0
WireConnection;344;3;341;0
WireConnection;423;0;342;0
WireConnection;345;0;423;0
WireConnection;345;1;344;0
WireConnection;345;2;343;0
WireConnection;346;0;344;0
WireConnection;346;1;343;0
WireConnection;435;0;444;0
WireConnection;365;0;345;0
WireConnection;352;0;346;0
WireConnection;434;0;435;0
WireConnection;367;0;369;0
WireConnection;367;1;398;0
WireConnection;367;2;366;0
WireConnection;433;0;434;0
WireConnection;432;0;433;0
WireConnection;415;0;367;0
WireConnection;360;0;415;0
WireConnection;436;1;432;0
WireConnection;408;0;406;0
WireConnection;431;0;433;0
WireConnection;431;1;436;0
WireConnection;411;0;408;0
WireConnection;411;1;419;0
WireConnection;425;0;121;0
WireConnection;425;1;431;0
WireConnection;425;2;433;0
WireConnection;404;0;411;0
WireConnection;404;2;425;0
ASEEND*/
//CHKSM=6F4F7B0A4052E8A45FAC395EB58DF16AD339C0F5
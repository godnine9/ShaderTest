
Shader "Test Shader"
{
	Properties
	{
		_startColor ("_statrColor",Range(0,1))=0
	}
	
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			float _startColor;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 position : SV_POSITION;
			};
			
			v2f vert ( appdata _in )
			{
				v2f o;
				o.position = UnityObjectToClipPos(_in.vertex);
				o.uv = _in.uv;
				return o;
			}
			
			float4 frag (v2f _in ) : SV_Target
			{
				float red = clamp(_in.uv.x, _startColor ,1);
				float green = clamp(_in.uv.y, _startColor ,1);
				float4 fragColor = float4(red,green,0,1);

				return fragColor;
			}
			ENDCG
		}
	}
}
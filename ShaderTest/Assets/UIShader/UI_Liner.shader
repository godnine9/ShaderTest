// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UILine"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_TintColor("TintColor", Color) = (0,0,0,0)
		_OutlineTintColor("OutlineTintColor", Color) = (0,0,0,0)
		_InnerlineTintColor("InnerlineTintColor", Color) = (0,0,0,0)
		_OutlineSize("OutlineSize", Range( 0 , 0.005)) = 0
		_InnerlineSize("InnerlineSize", Range( 0 , 0.005)) = 0
		_OutLineSample("OutLineSample", 2D) = "white" {}
		_InnerLineSample("InnerLineSample", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _OutLineSample;
			uniform float4 _OutLineSample_ST;
			uniform float4 _OutlineTintColor;
			uniform float4 _MainTex_ST;
			uniform float4 _TintColor;
			uniform float _OutlineSize;
			uniform sampler2D _InnerLineSample;
			uniform float4 _InnerLineSample_ST;
			uniform float4 _InnerlineTintColor;
			uniform float _InnerlineSize;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				float2 uv_OutLineSample = IN.texcoord.xy * _OutLineSample_ST.xy + _OutLineSample_ST.zw;
				float4 OutlineTexture105 = ( tex2D( _OutLineSample, uv_OutLineSample ) * _OutlineTintColor );
				float2 uv_MainTex = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode11 = tex2D( _MainTex, uv_MainTex );
				float4 BaseTexture147 = ( ( tex2DNode11 + _TextureSampleAdd ) * _Color * IN.color );
				float Alpha30 = tex2DNode11.a;
				float2 appendResult86 = (float2(_OutlineSize , 0.0));
				float2 uv053 = IN.texcoord.xy * float2( 1,1 ) + appendResult86;
				float2 appendResult91 = (float2(( _OutlineSize * -1.0 ) , 0.0));
				float2 uv090 = IN.texcoord.xy * float2( 1,1 ) + appendResult91;
				float2 appendResult88 = (float2(0.0 , _OutlineSize));
				float2 uv087 = IN.texcoord.xy * float2( 1,1 ) + appendResult88;
				float2 appendResult93 = (float2(0.0 , ( _OutlineSize * -1.0 )));
				float2 uv092 = IN.texcoord.xy * float2( 1,1 ) + appendResult93;
				float OutlineAlpha66 = step( 0.0 , ( Alpha30 - max( max( tex2D( _MainTex, uv053 ).a , tex2D( _MainTex, uv090 ).a ) , max( tex2D( _MainTex, uv087 ).a , tex2D( _MainTex, uv092 ).a ) ) ) );
				float4 lerpResult73 = lerp( OutlineTexture105 , ( BaseTexture147 * _TintColor ) , OutlineAlpha66);
				float2 uv_InnerLineSample = IN.texcoord.xy * _InnerLineSample_ST.xy + _InnerLineSample_ST.zw;
				float4 InnerTexture141 = ( tex2D( _InnerLineSample, uv_InnerLineSample ) * _InnerlineTintColor );
				float2 appendResult112 = (float2(_InnerlineSize , 0.0));
				float2 uv0117 = IN.texcoord.xy * float2( 1,1 ) + appendResult112;
				float2 appendResult113 = (float2(( _InnerlineSize * -1.0 ) , 0.0));
				float2 uv0119 = IN.texcoord.xy * float2( 1,1 ) + appendResult113;
				float2 appendResult114 = (float2(0.0 , _InnerlineSize));
				float2 uv0118 = IN.texcoord.xy * float2( 1,1 ) + appendResult114;
				float2 appendResult115 = (float2(0.0 , ( _InnerlineSize * -1.0 )));
				float2 uv0116 = IN.texcoord.xy * float2( 1,1 ) + appendResult115;
				float InnerAlpha130 = ( Alpha30 * ( 1.0 - ( tex2D( _MainTex, uv0117 ).a * tex2D( _MainTex, uv0119 ).a * tex2D( _MainTex, uv0118 ).a * tex2D( _MainTex, uv0116 ).a ) ) );
				float4 lerpResult136 = lerp( lerpResult73 , InnerTexture141 , InnerAlpha130);
				float4 FinalTexture150 = lerpResult136;
				float FinalAlpha153 = ( Alpha30 + ( 1.0 - OutlineAlpha66 ) );
				float4 appendResult59 = (float4(FinalTexture150.rgb , FinalAlpha153));
				
				half4 color = appendResult59;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17800
2261;149;1334;774;2658.915;-1082.122;1.376609;True;False
Node;AmplifyShaderEditor.CommentaryNode;101;-2443.934,1252.449;Inherit;False;1885.051;905.118;Comment;22;66;58;56;57;79;77;78;69;92;87;68;67;55;90;53;91;93;88;86;96;95;82;OutlineLineAlpha;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-2393.934,1787.727;Inherit;False;Property;_OutlineSize;OutlineSize;3;0;Create;True;0;0;False;0;0;0.000796358;0;0.005;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;-2268.719,1941.478;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;108;-444.8504,1259.472;Inherit;False;1885.051;905.118;Comment;20;109;110;111;112;113;114;115;116;117;118;119;120;121;122;123;127;130;131;132;133;InnerLineAlpha;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;95;-2263.299,1539.91;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;88;-2076.485,1708.154;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;91;-2074.526,1550.245;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;86;-2086.833,1405.104;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;93;-2085.261,1954.052;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;109;-411.3481,1779.412;Inherit;False;Property;_InnerlineSize;InnerlineSize;4;0;Create;True;0;0;False;0;0;0.00240895;0;0.005;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;8;-2523.142,-891.5965;Inherit;False;1437.033;568.0354;Comment;10;147;27;156;30;12;43;42;41;11;100;Base Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;110;-280.7131,1531.595;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;90;-1899.276,1519.449;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;-286.1331,1933.163;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;92;-1891.575,1951.181;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;100;-2505.053,-824.7158;Inherit;False;0;0;_MainTex;Shader;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;87;-1898.208,1724.174;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;53;-1897.964,1363.714;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;113;-91.93996,1541.93;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;55;-1640.483,1322.169;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;112;-104.2471,1396.789;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;41;-2206.66,-573.7152;Inherit;False;0;0;_TextureSampleAdd;Pass;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;115;-102.675,1945.737;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;67;-1637.047,1524.116;Inherit;True;Property;_TextureSample2;Texture Sample 2;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;69;-1639.891,1933.35;Inherit;True;Property;_TextureSample5;Texture Sample 5;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;114;-93.89919,1699.839;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;68;-1637.181,1719.318;Inherit;True;Property;_TextureSample3;Texture Sample 3;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;11;-2327.475,-829.6731;Inherit;True;Property;_TextureSample4;Texture Sample 4;2;0;Create;True;0;0;False;0;-1;None;de92c96f8570409488905bdbb9eedaaf;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;78;-1320.014,1861.171;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;156;-1920.583,-576.5823;Inherit;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;118;84.37793,1715.859;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;77;-1313.812,1664.541;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;117;84.62195,1355.399;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;30;-2036.117,-734.8272;Inherit;False;Alpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;119;83.30994,1511.134;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;116;91.01099,1942.866;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;57;-1330.717,1469.057;Inherit;False;30;Alpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;43;-1705.645,-527.4384;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;121;342.6949,1925.035;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-1843.984,-825.6034;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;42;-1713.211,-713.9317;Inherit;False;0;0;_Color;Shader;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;79;-1165.688,1743.008;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;107;-2436.611,647.6635;Inherit;False;1009.052;514.7475;Comment;4;103;102;104;105;Outline Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;123;342.1029,1313.854;Inherit;True;Property;_TextureSample7;Texture Sample 7;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;120;345.4049,1711.003;Inherit;True;Property;_TextureSample6;Texture Sample 6;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;122;345.5389,1515.801;Inherit;True;Property;_TextureSample8;Texture Sample 8;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;11;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;56;-1125.212,1456.768;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;102;-2386.611,697.6635;Inherit;True;Property;_OutLineSample;OutLineSample;5;0;Create;True;0;0;False;0;-1;None;f56dd87d67818a3418f3edb7e0e210f8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;131;704.6818,1693.804;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-1487,-829.1334;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;103;-2360.919,955.411;Inherit;False;Property;_OutlineTintColor;OutlineTintColor;1;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;137;-445.206,672.803;Inherit;False;1009.052;514.7475;Comment;4;141;140;139;138;Inner Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;147;-1335.18,-834.1759;Inherit;False;BaseTexture;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;144;-785.3029,-887.5029;Inherit;False;1177.788;673.7849;Comment;10;150;136;135;73;142;21;75;106;19;148;TextureCalculation;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;104;-1893.55,904.0002;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;127;851.8689,1527.742;Inherit;False;30;Alpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;139;-369.5139,980.5502;Inherit;False;Property;_InnerlineTintColor;InnerlineTintColor;2;0;Create;True;0;0;False;0;0,0,0,0;0.3490566,0.3490566,0.3490566,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;58;-951.1541,1456.498;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;132;905.6818,1692.804;Inherit;True;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;138;-395.206,722.803;Inherit;True;Property;_InnerLineSample;InnerLineSample;6;0;Create;True;0;0;False;0;-1;None;a8d3a05576570d741a88dfcb43f96854;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;19;-749.7717,-601.7722;Inherit;False;Property;_TintColor;TintColor;0;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;143;-2473.397,-46.68559;Inherit;False;912.3309;456.2063;Comment;5;97;153;98;99;152;Alpha Calculation;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;148;-745.2845,-798.6899;Inherit;False;147;BaseTexture;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;133;1097.663,1594.719;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;105;-1678.559,936.7162;Inherit;False;OutlineTexture;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;140;97.855,929.1396;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;66;-792.1572,1454.731;Inherit;False;OutlineAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;152;-2416.019,282.7305;Inherit;False;66;OutlineAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-432.8669,-716.1565;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;106;-534.0385,-552.1234;Inherit;False;105;OutlineTexture;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;141;312.8461,961.8555;Inherit;False;InnerTexture;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;130;1190.429,1446.416;Inherit;False;InnerAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;-736.0384,-370.5736;Inherit;False;66;OutlineAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;99;-2162.074,266.8197;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;73;-235.1116,-576.4453;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;142;-334.751,-428.3961;Inherit;False;141;InnerTexture;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;135;-333.2327,-319.82;Inherit;False;130;InnerAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;97;-2391.303,53.99746;Inherit;False;30;Alpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;136;-38.08098,-498.2197;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;98;-2028.155,67.1218;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;153;-1880.619,68.8396;Inherit;False;FinalAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;155;-620.0633,-34.7295;Inherit;False;869.2437;372.2335;Comment;4;151;59;29;154;FinalOutput;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;150;178.9709,-498.5956;Inherit;False;FinalTexture;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;154;-559.2638,222.504;Inherit;False;153;FinalAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;-570.0633,15.2705;Inherit;False;150;FinalTexture;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;59;-231.3062,45.47147;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;29;72.18042,19.61012;Float;False;True;-1;2;ASEMaterialInspector;0;4;UILine;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;True;2;False;-1;True;True;True;True;True;0;True;-9;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;0
WireConnection;96;0;82;0
WireConnection;95;0;82;0
WireConnection;88;1;82;0
WireConnection;91;0;95;0
WireConnection;86;0;82;0
WireConnection;93;1;96;0
WireConnection;110;0;109;0
WireConnection;90;1;91;0
WireConnection;111;0;109;0
WireConnection;92;1;93;0
WireConnection;87;1;88;0
WireConnection;53;1;86;0
WireConnection;113;0;110;0
WireConnection;55;1;53;0
WireConnection;112;0;109;0
WireConnection;115;1;111;0
WireConnection;67;1;90;0
WireConnection;69;1;92;0
WireConnection;114;1;109;0
WireConnection;68;1;87;0
WireConnection;11;0;100;0
WireConnection;78;0;68;4
WireConnection;78;1;69;4
WireConnection;156;0;41;0
WireConnection;118;1;114;0
WireConnection;77;0;55;4
WireConnection;77;1;67;4
WireConnection;117;1;112;0
WireConnection;30;0;11;4
WireConnection;119;1;113;0
WireConnection;116;1;115;0
WireConnection;121;1;116;0
WireConnection;27;0;11;0
WireConnection;27;1;156;0
WireConnection;79;0;77;0
WireConnection;79;1;78;0
WireConnection;123;1;117;0
WireConnection;120;1;118;0
WireConnection;122;1;119;0
WireConnection;56;0;57;0
WireConnection;56;1;79;0
WireConnection;131;0;123;4
WireConnection;131;1;122;4
WireConnection;131;2;120;4
WireConnection;131;3;121;4
WireConnection;12;0;27;0
WireConnection;12;1;42;0
WireConnection;12;2;43;0
WireConnection;147;0;12;0
WireConnection;104;0;102;0
WireConnection;104;1;103;0
WireConnection;58;1;56;0
WireConnection;132;1;131;0
WireConnection;133;0;127;0
WireConnection;133;1;132;0
WireConnection;105;0;104;0
WireConnection;140;0;138;0
WireConnection;140;1;139;0
WireConnection;66;0;58;0
WireConnection;21;0;148;0
WireConnection;21;1;19;0
WireConnection;141;0;140;0
WireConnection;130;0;133;0
WireConnection;99;0;152;0
WireConnection;73;0;106;0
WireConnection;73;1;21;0
WireConnection;73;2;75;0
WireConnection;136;0;73;0
WireConnection;136;1;142;0
WireConnection;136;2;135;0
WireConnection;98;0;97;0
WireConnection;98;1;99;0
WireConnection;153;0;98;0
WireConnection;150;0;136;0
WireConnection;59;0;151;0
WireConnection;59;3;154;0
WireConnection;29;0;59;0
ASEEND*/
//CHKSM=A84A19A58E957466313C9643CD708AE7DE12AFDB